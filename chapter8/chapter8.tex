% !TeX root = ../dissertation.tex
\chapter{Conclusions}
\input{"chapter8/litreview.tex"}
\label{c:chapter8}
% 
% 
% 
Development of a finite element models for biological applications is a very complicated process. Unlike that in typical engineering research subjects, there is still very little knowledge of the material properties. Another issue is lack of proper definition of the boundary conditions. Constraints are mostly explicit when analysing engineering objects such as concrete beams or steel frames under different loading conditions. 
However, in a living body it is challenging to accurately define loads. 
Model geometry suffers from inaccuracies in scanning as well as segmentation process. 
Results from such models could potentially include an increasing number of errors since the data collected from each stage of the 
model creation encompasses high uncertainty data~\citep{campoli_effects_2014}.
Available information is usually subject-specific or gathered for a relatively small population of subjects and cannot be generalised. 
Subsequently, even if one manages to overcome the aforementioned obstacles, there is a need for validation of such models, which will also have to deal with similar difficulties. Over the years, many frameworks have been developed to quantify such uncertainty levels, which might greatly increase the credibility of computational models~\citep{wille_uncertainty_2016}. CT scanning on living horses limbs is still too cumbersome to be used as a standard diagnostic tool. To the best author's knowledge, there is a limited number of facilities that can perform CT scanning for horses without the necessity for anaesthesia. 
Nevertheless, the outcomes of this study can bring new insight in the area of catastrophic injuries to improve the welfare of the thoroughbred racehorse. The successful application of developed framework would enable the introduction of interventions for veterinary practitioners, such as suggestions for training regimes based on known risk factors for lateral condylar fractures that could reduce the probability of fracture in racehorses. \\
% 

The first task of this thesis was to build understanding of the current state of the art of FEM techniques for analysing bone fracture resistance. The aim was to identify the key features of a patient-specific framework capable of incorporating accurate 3D data from CT-scanning, simulating long term bone response to the proposed training regime and ultimately predicting the risk of fracture. The difficulty was to establish a balance between the most essential components to provide accurate predictions and what could practically be executed in an patient-specific modelling routine within a limited time window. This was achieved by choosing efficient computational methods and the establishment of a consistent low level of complexity for the models to build a robust framework. \\
% 
% 

The procedure of building a finite element models for analysing bones always has to start with data acquisition. In Chapter~\ref{c:chapter3}, a three dimensional imaging technique known as QCT scanning was applied to obtain accurate geometries of the equine bones and estimate their mineral density. Furthermore, two novel methods for mapping density data onto finite element meshes were proposed. The first method - $L_2$-projection was used to investigate density gradients at common site of fracture for three groups of equine limbs. Although the method has a potential of robustly identifying possible fracture risk factor (like high density gradient), it was found that small cohort of specimens was insufficient to make any meaningful conclusions. The second presented method - meshless MWLS does not use standard FEM discretisation and provides density field approximation with high regularity.
% at least $C^1$ continuity. 
This essential feature allows further for calculation of material forces in heterogeneous materials and ultimately efficient implicit crack propagation simulation within configurational mechanics framework. Nevertheless, the accuracy of the presented approaches still has to be validated experimentally, for example, in the prediction of strains in the loaded bone specimen.\\
% 
% 

To simulate long-term bone response to mechanical loading a formulation for bone remodelling using open system thermodynamics framework was implemented. It has been shown that chosen approach is capable of predicting realistic density profiles in hard tissues in response to over or underload. Benchmark examples demonstrated stability and scalability of the implemented monolithic solution scheme. Two different methods of calculating consistent tangent matrix were presented. Additionally, the performance of the model was compared with a simple topology optimisation algorithm proposed in Appendix~\ref{c:topology_op} \\
% 
% 
% 

With accurate bone density profiles and material properties derived through QCT scanning or bone remodelling simulations, the next natural step in the proposed framework is to estimate the fracture resistance. In Chapters~\ref{c:chapter5} and \ref{c:chapter6} two disparate approaches were proposed for simulating crack propagation and estimating critical load factors in heterogeneous bones. \\
The first one is the popular phase-field method, where crack is represented by a smooth damage variable leading to a phase-field approximation of the variational formulation for brittle fracture. To trace the nonlinear response the implementation was augmented with an adaptive arc-length control method based on the rates of the internal and the dissipated energy. Furthermore, the potential of three different methods for obtaining consistent tangent matrix is explored. The heterogeneity of the material can be captured straightforwardly by using spatially varying material properties as local variables. The performance of the proposed phase field formulation of fracture was demonstrated by means of representative numerical examples. \\
In the second discrete approach for fracture, configurational forces are the driver for crack propagation. In order to evaluate correctly the driving forces at the crack front for inhomogeneous materials it was shown that it is necessary to have a spatially smooth density field, with higher regularity.%
% than if the field is directly approximated on the finite element mesh. 
Therefore, density data was approximated as a smooth field using a Moving Weighted Least Squares method. Moreover, to improve the accuracy of the calculated nodal crack driving forces, singular stress state was modelled using Quarter Point element concept. The convergence and validation tests were conducted. \\
% 
% 
% was adopted into the proposed framework
% 

In the penultimate chapter of this work, the full potential of the developed framework was assessed in the form of practical numerical examples. First analysis of a proximal femur with implant showed that utilised formulation for bone remodelling has the the capability of predicting bone density patterns that are in good comparison with structural arrangement observed on CT scans. 
Subsequently, a comparative study was conducted in order to evaluate the performance of both implemented methods for approximating fracture in heterogeneous materials. It was concluded that in the current state of development, configurational force approach is more suitable for the framework. It introduces less parameters and works effectively on coarse meshes. 
In the last section, numerical examples demonstrated the performance and accuracy of the proposed framework for analysing equine MC3 bone. Numerical convergence was demonstrated for all examples and the use of singularity elements was shown to further improve the rate of convergence. However, it was also confirmed that improved accuracy of the stress at the tip had no impact on the crack propagation analysis and the resulting crack path. The final example, demonstrated how mechanical loading and subsequent adaptation influence the resistance to bone fracture. Ultimately, it was demonstrated that the proposed framework can be a useful tool in understanding fractures in bone and preventing catastrophic fractures. \\

In summary, the goal of this study was to develop a robust numerical framework for estimation of a fracture risk of MC3 bone followed by adaptation. 
The main achievements of this work are the following:
\begin{itemize}
  \item Incorporation of efficient and accurate mapping strategies to represent heterogeneous bone material properties for finite element models. 
  \item Efficient implementation of bone adaptation algorithm based on open system thermodynamics to predict density levels in response to exercise. 
  \item Deployment of three-dimensional phase-field formulation for brittle fracture with robust arc-length control. 
  \item Novel application of configurational mechanics for modelling fracture extended to include the influence of heterogeneous bone density distribution.
  \item Assessment of the potential of developed framework in evaluating bones' propensity to failure.
\end{itemize}

%  A finite element framework for crack propagation in heterogenous and adapting materials.
%  Fully implicit formulation for modelling the evolving crack front.

%  Moving Weighted Least Squares approximation approach for mapping data onto finite element meshes.


% Despite certain limitations, it was demonstrated that the implemented open systems thermodynamics framework allows for simulation of a natural behaviour of hard biological tissues. 
% The proposed computational method may have a great potential to identify the risk of fracture related to changes in bone mineral density. 
% Understanding the effects of changing release energy in response to bone adaptation can help developing training regimes that reduces the liability of fatal injury. 
% Furthermore, the model uses very few parameters which can be experimentally determined in a practical manner increasing the effectiveness of this approach.\\ 




% %
% This contribution also investigated the application of a meshless MWLS method in approximating the density data on FE models. Validation of analytical field mapped on a simple mesh and comparison with LS method on mapping data from CT scanning was conducted and proved that MWLS can be a suitable technique for the approximation of density field, even with strong gradients. 
% Nevertheless, the accuracy of the presented approach (MWLS) still has to be validated experimentally, for example, in the prediction of strains in the loaded bone specimen. \\
% %
% A novel method in quantifying bone fracture propensity was presented. By scanning the bone, running the bone adaptation simulation and subsequently introducing a notch in the geometry at particular time steps, it can be observed from the values of configurational forces how loading the bone influences its resistance to fracture. 
% This framework, can be potentially a very useful tool in diagnostic of fractures and eventually prevention of catastrophic failures. 
% Moreover, the code used for all numerical analyses was developed keeping in mind scalability and robustness. 
% Entire framework can be executed on parallel computer system in order to fit in an efficient patient-specific modelling routine that can handle many patients within a practical time window. \\

% Numerical examples demonstrated the accuracy of the proposed framework. 
% Convergence has been validated for all examples, in particular, the use of quarter point elements was shown to improve the convergence of calculated release energy rates. However, it was also confirmed that improved accuracy of the stress at the tip has no impact on the crack propagation analysis.


% In summary the main accomplishment this work are the following:  
% \begin{itemize}
%   \item Two density mapping methods:
%   \item Implementation of bone remodelling. In 3D, fully parallel with arbitrary order of approximation
%   \item Monolithic solver for phase-field 
%   \item Fracture in heterogeneous materials using configurational mechanics 
%   \item Numerical tests 
% \end{itemize}


\section{Limitations and future work} 

% the configurational-force-driven model is of limited applicability due to its problems with crack initiation and branching.

The methods presented in this work have several important limitations. 
Considerably more work has to be done to determine bone loading, since using simplified input results in unrealistically low densities in non load-bearing regions. Multiple load cases can be critical in modelling bone adaptation~\citep{geraldes_consideration_2016}. 
In the future, the forces should be obtained by using gait data and musculoskeletal analysis~\citep{delp_opensim:_2007} combined with e.g. finite element mortar contact formulation~\citep{athanasiadis_mortar_2018}. Another method for predicting loading conditions is to solve an~inverse problem to bone adaptation. Promising results have been reported in that field by using machine learning methods like Neural Networks~\citep{campoli_computational_2012}. 
Another limitation of the proposed scheme is the requirement for manual generation and meshing of bone geometry models that often requires commercial software. In future, it would be beneficial to explore methods for automatic segmentation and meshing as proposed e.g. in \cite{trabelsi_patient-specific_2011}. The model generation should be fully automatic to handle many specimens within a limited time frame.
The current fracture approximation approaches do not take into account nonlinear cohesive mechanisms like collagen fibre bridging at the crack tip~\citep{yang_fracture_2006}. Therefore, the calculated release energy or load factor with presented approaches might be overestimated. More research is also required to calibrate material parameters, in particular those regarding constitutive relations for bone adaptation. 

% very limited number of specimens 

% loading conditions are simplified 

% material parameters are not calibrated 

% subject-specific gait analysis using an inverse dynamics approach.

% the accuracy of the presented approach (MWLS) still has to be validated experimentally, for example, in the prediction of strains in the loaded bone specimen. \\

% no microstructure taken into account

% (adjusted microstructure can increase macroscopic resistance to failure due to crack growth ) Bioinspired Design Criteria for Damage‐Resistant Materials with Periodically Varying Microstructure

% model parameters are not calibrated

% everything is subject specific 

% phase-field does not have adaptive mesh refinement

% phase-field can be unstable (non-convex problem)

% homogeneous gc in configurational mechanics approach 

% no inertial forces are taken into account Thus, their introduction is an essential compo- nent of a comprehensive approach to fracture that is able to deliver insight for structural design where the phenomenon of fracture is relevant. (it has been argued that they are very important \citep{steinke_comparative_2016})

% adaptive mesh refinement 

% shape is not preserved during fracture of the bones (surface cannot be defined)

% (maybe define STL and allow vertices so slide only within it)

% % It is worth to note, that currently shape preservation constrains are limited only to planar surfaces, therefore they are not applicable to complex geometries like bones. 

% incorporate automatic segmentation and meshing (give references)

% contact analysis (simple, automatic, don't want to add more complexity)

% solve inverse problem to calculate to calculate loading

% mechanical testing for material properties 

% ct-scanning of the MC3 for a horse in training to calibrate model parameters


% The two main components of subject-specific FE models- model geometry and material properties, can be derived from computed tomography (CT) datasets. Generation of the three-dimensional (3D) geometry of a bone segment from CT data might be complex and time-consuming depending complexity of the considered bone.

% work on efficiency and automatisation of the framework 

% it would beneficial to test the sensitivity of the load-displacement curves on the placement and the size of the initial crack. 

%Overall, the presented examples of density evolution, at this point are not sufficient to make any practical conclusions about the remodelling of the metacarpal bones. \\


%Calculation of release energy rate does not give any insights into quantitative strength of the bones' structure. Future implantation on an implicit crack propagation for heterogeneous bodies will allow to trace a full dissipative loading path. The process of determining detailed musculoskeletal loads using kinematic data and external forces is called inverse dynamic analysis

%Another limitation of the study is that only one load case is considered. 
%However, it is important to note that while simulating bone functional adaptation a detailed knowledge of the actual loading situation is elementary. By assuming that all three cases occur simultaneously in some loading conditions forces could balance each other.

%The model is not yet calibrated and validated against patient data, however, it provides valuable insight to the bone remodelling behaviour for equine metacarpal and is capable of qualitatively comparing different loading.

%bone remodelling is not purely load-driven. Possible causes for this could be that also other mechanisms, for example, calcium homeostasis, influence normal bone remodelling and thus bone is never fully load adapted. as suggested in~\citep{christen_bone_2014}. The authors of previously mentioned publication also suggests that approximately 50\% of the bone structure might be determined by mechanical loading.


%No previous study has investigated release energy of brittle fracture in heterogeneous bodies like bones. 
%In the future we might use a micro CT scanning as ot was shown that it is currently the most accurate method to estimate bone strength. 
%We are hoping that when we refine the model we could confirm that bone remodelling may increase the propensity to cracks and find potential practical applications in the fracture prevention.
% this research can be easily translated into human applications 
%Presented framework could be also combined with other risk factors~\citep{georgopoulos_risk_2017} which should improve the clinical assessment of fracture risk
%The correlation between bone adaptation, high intense training and stress fractures is still poorly understood. The promising results of this study offer a novel framework to simulate  changes in the bone structure as a result of loading and quantify the risk of fatal injury. 
%In conclusion, this study has shown that fracture resistance in the equine metacarpal bone can be numerically quantified. 
%Understanding the effects of changing release energy in response to bone remodelling will help in the development of training regimes that reduces the risk of fatal injury in racehorses
%This approach might become especially handy to analyse whether a discovered crack in the equine bone is liable to propagate under certain loading conditions. 
%model with the capability to simulate the bone density growth and resorption processes due to mechanical stimuli
%A reduction in this speciﬁc type of fracture would have a signiﬁcant global impact on the number of Thoroughbred redracehorses subjected to euthanasia as a result of in jury incurred duringracing
%has a massive welfare and economic impact on horse racing

%A better understanding of the influence of race training on subchondral bone remodelling is essential to understanding the pathophysiology of subchondral bone injury.

%Therefore, the development of computational tools with the capability to estimate bone adaptation when prosthetic devices are used has a remarkable importance, since these processes may contribute to implant success or failure. In this scenario, the finite-element method (FEM) has been playing a key role, being used to study and evaluate the mechanical behaviour of prosthetic devices

%The flexibility of the hierarchic finite element is fully utilised, which permits the use of arbitrary order of approximation leading to accurate results for relatively coarse meshes. The developed computational framework is implemented in our group's FE software, MoFEM (Mesh-Oriented Finite Element Method). \\

%Moreover, the application of the study proposes a computational assessment tool for veterinary specialists, focused on the simulation of a healthy femur and a femur with an implanted prosthesis submitted to loads.

%The main objective advocated in this contribution is the setting up of a modelling framework relying on the thermodynamics of irreversible processesLorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum velit a nibh consequat cursus id at quam. Maecenas a elit lacinia, porttitor sapien elementum, vestibulum erat. Morbi euismod sit amet augue quis fermentum. Donec vitae ipsum sit amet tortor bibendum porttitor. Donec velit dui, lobortis ut malesuada nec, iaculis ac elit. Pellentesque elementum nulla
 