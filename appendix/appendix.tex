%!TeX root = ../dissertation.tex
\chapter{Topology optimisation}%
\label{c:topology_op}%
% 
Topology optimisation is a mathematical method for optimising the material distribution within a domain, for a given set of boundary conditions and constraints to maximise the performance of the structure. Over the years many different approaches have been developed, including density penalisation, level set, topological derivative, phase-field or evolutionary methods. An extensive overview can be found in \cite{sigmund_topology_2013}.  
\section{Implementation}
The topology implementation problem presented herein is based on popular \emph{Solid Isotropic Material with Penalization} (SIMP) \citep{bendsoe_theory_2003} method, where the objective is to minimize the compliance of the considered domain $\Omega$ as follows
\begin{equation}
  \begin{aligned}
  \min _{\rho}: & \quad c(\rho)=\sum_{e=1}^{n} \mathbf{u}_e^\textrm{T} \mathbf{K}_e(\rho) \mathbf{u}_e \\
  \textrm{s.t. }: & \sum_{e=1}^n \int_\Omega \rho \, \mathrm{dV}_e = f V_0 \\ 
  : & \quad 0 < \rho_\textrm{min} \leq \rho \leq 1 \\
  : & \quad \mathbf{K}(\rho) \mathbf u = \mathbf{f}
  \end{aligned}
  \label{eq:to_problem}
\end{equation}
where $\mathbf K$ is the global stiffness matrix, $\mathbf{u}$ vector if displacements, $\mathbf f $ is the nodal force vector, $\rho$ is the element density (design variable), $V_e$ and $V_0$ is the element volume and volume of the entire domain, respectively. $f$ is the desired volume fraction. The element stiffness $\mathbf K_e$ is calculated as follows: 
\begin{equation}
  \mathbf K_e = \int_\Omega (\rho)^p \nabla_\mathbf{X} \pmb{\Phi}^\textrm{T}  \mathcal D \nabla_\mathbf{X} \pmb{\Phi} \, \mathrm{dV}_e
\end{equation}
where $p$ is the penalization coefficient (typically $p=3$) and $\mathcal D$ is the 4th order elasticity tensor. The optimisation problem in ~\ref{eq:to_problem} is solved with Lagrange multipliers method. The derivative of the objective function with respect to design variables has the form:
\begin{equation}
  \frac{\partial \mathcal{L}}{\partial \rho}= - \mathbf{u}_e^\textrm{T} \frac{\partial \mathbf{K}_e }{\partial \rho} \mathbf{u}_e+ \lambda_{\mathrm{b}} V
\end{equation} 
where $\mathcal{L}$ is the Lagrangian and $\lambda_{\mathrm{b}}$ is the Lagrange multiplier. The condition of optimality can be expressed as follows:
\begin{equation}
  \renewcommand\arraystretch{2.0}
  \left\{\begin{array}{l}
    {\dfrac{\partial \mathcal{L}}{\partial \rho} \geq 0 \text { if } \rho=\rho_{\min }} \\
    {\dfrac{\partial \mathcal{L}}{\partial \rho}=0 \text { if } \rho_{\min }<\rho<1} \\
    {\dfrac{\partial \mathcal{L}}{\partial \rho} \leq 0 \text { if } \rho=1}
    \end{array}\right.
\end{equation}
Furthermore, the Optimality Criteria algorithm can be applied with a heuristic iteration scheme for the design variables defined as:     
\begin{equation}
\rho^{n+1} = \left( \frac{1}{\lambda_{\mathrm{b}} V} \mathbf u^\textrm{T} \frac{\partial \mathbf K_e}{\partial 
\rho} \mathbf u  \right)^\eta \rho^n
  \label{eq:op_crit}
\end{equation}
where $n$ is the iteration number and $\eta$ is a numerical damping coefficient, typically $\eta = 0.5$. Additionally  a \emph{move} limit is imposed (often $m=0.2$) at each iteration to ensure stability of the algorithm. At each iteration $\rho^{n+1}$ has to satisfy the following inequality:
\begin{equation}
  \max{ (\rho^n - m, \rho_\textrm{min} )} \leq \rho^{n+1} \leq \min{ (\rho^n + m, 1 )}
  \label{eq:move_op}
\end{equation}
The nonlinear solution for multiplier $\lambda_{\mathrm{b}} $ is obtained through bisection method. $\rho_\text{min}$ is imposed to avoid zero stiffness elements, typically $\rho_\text{min} = 0.001$. 
%
\section{Density filtering}
To prevent the checkerboard pattern and ensure mesh-independency of the solution for topology optimisation problems, typically a filtering techniques on density $\rho$ or sensitivity  
$\partial c / \partial \rho$ are applied \citep{bourdin_filters_2001}. Most of the methods in the literature require information about the neighbour elements, which makes them difficult to partition and parallelise for efficient multiple CPU computations. Therefore, more recently a new family of filters based on Helmholtz-type differential equations has emerged  \citep{lazarov_filters_2011}. \\
The basic idea behind filters inspired by Helmholtz PDE is to solve the following differential equation:
\begin{equation}
  -\lambda_l \nabla_\mathbf{X}^2 \tilde{\rho} + \tilde{\rho} = \rho
  \label{eq:helmholtz_rho}
\end{equation}
with Neumann boundary condition imposed on the surface of the domain as follows:
\begin{equation}
  \frac{\partial \tilde{\rho}}{\partial \mathbf n} = 0
\end{equation}
where $\tilde{\rho}$ is nodal density and $\lambda_l$ can be considered as length scale parameter. 
After finding the densities $\rho$ for each element, that satisfy the constrains in Eq.~\ref{eq:to_problem} through bisection method, the PDE in Eq.~\ref{eq:helmholtz_rho} can be solved with standard FEM discretisation resulting in the following system of linear equations for the unknown values of the filtered density field:
\begin{equation}
  \mathbf K_f \tilde{\mathbf \rho} = \mathbf p_f
\end{equation}
where the matrix $\mathbf K_f $ and vector $\mathbf p_f$ is obtained as follows:
\begin{equation} 
  \begin{aligned}
    \mathbf K_f = & \sum_{e=1}^n \int_\Omega  \lambda_l  \nabla_\mathbf{X} \pmb{\Phi}^\textrm{T} \nabla_\mathbf{X} \pmb{\Phi} + \pmb{\Phi}^\textrm{T}  \pmb{\Phi}  \, \mathrm{dV}_e\\
    \mathbf p_f = & \sum_{e=1}^n \int_\Omega \pmb{\Phi}^\textrm{T} \rho  \, \mathrm{dV}_e
  \end{aligned}
\end{equation}
Note that with this formulation non-constant density within the elements can be used. The performance of the approach is demonstrated in the next section in the form of representative numerical examples.
%
%
\section{Numerical examples}
%
In this section the capabilities of the discussed topology optimisation are presented. Two cantilever problems are considered: in 2D and 3D. The loads and the domain dimensions are shown in Figure~\ref{fig:topo_bc_2D_3D}. The meshes consist of 1385 and 39761 structured quadratic tetrahedral elements for 2D and 3D, respectively. Note that in the realm of hierarchical approximation basis, the order on the elastic and density filtering problems can be set independently. The problem parameters are: Young's modulus $E=1000$, Poisson ratio $\nu = 0.1$, \emph{move} limit $m=0.2$, penalty parameter $p=3$ and desired volume fraction $f=0.3$. 
%
%
\begin{figure}
	\begin{centering}
		\includegraphics[width=10cm]{appendix/Figures/topo_bc_2D_3D.png}
		\caption{Domain and boundary conditions for 2D and 3D examples. The dimensions are: 10x5 for 2D case and 10x5x5 for 3D case. In 2D unit nodal force is applied and in 3D spatially varying line load on bottom right edge, such that: $f(x,z,y) = 10 - z^2$.} 
		\label{fig:topo_bc_2D_3D}
	\end{centering}
\end{figure}
%
The results for 2D cantilever for different values of length scale parameter $\lambda_l$ are shown in Figure~\ref{fig:topo_2D_lambda0}. 
For $\lambda_l = 0$, which basically means no filtering, as expected - an unstable checkerboard like solution is obtained. In the following cases it can be noticed that by decreasing $\lambda_l$ finer and sharper truss-like elements can be obtained. The results with $\lambda_l = 0.1$ are in good agreement with the MATLAB code demonstrated in \cite{sigmund_99_2001}. \\
%
%
%
In the second example, inspired by ~\cite{aage_parallel_2013}, a 3D cantilever design problem is analysed. The parameters chosen are the same as in 2D case, apart from length scale parameter, where $\lambda_l = 0.1$ is used. The results in Figure~\ref{fig:topo_3D_evo} show every 5th iteration of the compliance minimisation steps. Note that the presented formulation can generate complex structure shapes with high resolution using relatively coarse meshes. 
%
%
\begin{figure}
  \begin{centering}
    \def\svgwidth{4cm} 	\input{appendix/Figures/density_scale.pdf_tex} \\
    \includegraphics[width=10cm]{appendix/Figures/topo_L0.png}\\
    a) \hspace{5cm} b)  \\
    \includegraphics[width=10cm]{appendix/Figures/topo_L1.png}\\
    c) \hspace{5cm} d)  \\
		\caption{Design variable for 2D cantilever after 20 iterations. a) $\lambda_l = 0$ b) $\lambda_l = 1$ c) $\lambda_l = 0.1$ d) $\lambda_l = 0.01$. } 
		\label{fig:topo_2D_lambda0}
	\end{centering}
\end{figure}
%
%
%
%
\begin{figure}
	\begin{centering}
    \includegraphics[width=14cm]{appendix/Figures/topo_every_5th.png}\\
		\caption{Evolution of design variable $\rho$ for every 5th iteration. Iso-volume plot showing $\rho > 0.35$. } 
		\label{fig:topo_3D_evo}
	\end{centering}
\end{figure} 
%
\\
The examples presented before utilised isotropic filtering i.e. $\lambda_l$ is a scalar. However, it is possible to replace that quantity with a tensor that will smear the design variable $\rho$ differently in desired directions. To demonstrate that feature, the same cantilever as previously is considered with scalar $\lambda_l = 0.2$ (see Fig.~\ref{fig:topo_aniso}a)) and a tensor expressed as:
%
%
\begin{equation}
  \lambda_{l}=\left[\begin{array}{ccc}
    {0.01} & {0} & {0} \\
    {0} & {0.01} & {0} \\
    {0} & {0} & {0.2}
    \end{array}\right]
    \label{eq:anis_tensor_filter}
\end{equation}
%
This will enforce filtering mainly in z-direction. The results after 50 iterations for both filters are demonstrated in Figure~\ref{fig:topo_aniso}.
%
\begin{figure}
	\begin{centering}
    \includegraphics[width=10cm]{appendix/Figures/topo_aniso.png}\\
    a) \hspace{5cm} b) 
		\caption{Resulting topology with a) isotropic filter $\lambda_l = 0.2$ and b) anisotropic filter (Eq.~\ref{eq:anis_tensor_filter}). Iso-volume plot showing for $\rho > 0.35$.} 
		\label{fig:topo_aniso}
	\end{centering}
\end{figure}
%
The demonstrated anisotropic filtering can be beneficial in case of designing structures for extrusion or additive manufacturing methods where strength of the parts depends highly on the direction of the material deposition. 
%
\section{Summary}
This contribution presented a simple implementation of the topological optimisation using SIMP algorithm. A Helmholtz-type PDE filter was utilised allowing for efficient parallelisation of the approach. The source code can be found in \cite{lewandowski_karol41_2018}.

%\chapter{Hierarchical shape functions}

%\chapter{Cell force identification}

%\chapter{Machine learning approach for identifying bone remodelling forces}

%\chapter{Importing VTK files into Unity for XR visualisations}


\chapter{List of publications}
\label{a:list_of_pub}

\subsection*{Journal article}
my list of journal articles

\subsection*{Conference articles}
my list of conference articles

\subsection*{Posters}
my list of posters

\subsection*{Source codes}
my list of bitbucket source code links
