all: final

.PHONY: dissertation clean ch-introduction ch-appendices ch-2 ch-3 ch-4 ch-5 ch-6 ch-7 ch-8
# Main ------------------------------------------------------------------------
final: 
	latexmk -pdf dissertation.tex

ch01:
	latexmk -pdf ch-introduction.tex
ch02:
	latexmk -pdf ch-2.tex
ch03:
	latexmk -pdf ch-3.tex
ch04:
	latexmk -pdf ch-4.tex
ch05:
	latexmk -pdf ch-5.tex

chAA:
	latexmk -pdf ch-appendices.tex

# Clean -----------------------------------------------------------------------
clean:
	git clean -f -X